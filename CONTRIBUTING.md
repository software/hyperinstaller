# Contributing to HyperInstaller

Any contributions through pull requests are welcome as this<br>
project aims to be a community based project to<br>
ease some Hyperbola installation steps.

Therefore, guidelines and style changes to the code might come into<br>
effect as well as guidelines surrounding error reporting and discussions.

## Branches

`master` is currently the default branch,<br>
and that's where all future feature work is being done,<br>
this means that `master` is a living entity and will<br>
most likely never be in a fully stable state.<br>
For stable releases, please see the tagged commits.

Patch releases will be done against their own branches,<br>
branched from stable tagged releases and will be<br>
named according to the version it will become on release.<br>
*(Patches to `v2.1.4` will be done on branch `v2.1.5` for instance)*.

## Discussions

Currently, questions, errors and suggestions should be<br>
reported through
[Hyperbola Project issue tracker](https://issues.hyperbola.info).<br>

## Coding convention

HyperInstaller's goal is to follow
[PEP8](https://www.python.org/dev/peps/pep-0008/)<br>
as best as it can with a minor exception.<br>

The exceptions to PEP8 are:

* HyperInstaller should always be<br>
  saved with **Unix-formatted line endings**<br>
  and no other platform-specific formats.

## Submitting Changes

Arch Installer contributors (2021-09-13):

* Anton Hvornum ([@Torxed](https://github.com/Torxed))
* Borislav Kosharov ([@nikibobi](https://github.com/nikibobi))
* demostanis ([@demostanis](https://github.com/demostanis))
* Dylan Taylor ([@dylanmtaylor](https://github.com/dylanmtaylor))
* Giancarlo Razzolini (@[grazzolini](https://github.com/grazzolini))
* j-james ([@j-james](https://github.com/j-james))
* Jerker Bengtsson ([@jaybent](https://github.com/jaybent))
* Ninchester ([@ninchester](https://github.com/ninchester))
* Philipp Schaffrath ([@phisch](https://github.com/phisch))
* Varun Madiath ([@vamega](https://github.com/vamega))
* nullrequest ([@advaithm](https://github.com/advaithm))

