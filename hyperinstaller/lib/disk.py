import glob
import pathlib
import re
import time
from typing import Optional

from .general import *
from .hardware import has_uefi
from .output import log

ROOT_DIR_PATTERN = re.compile('^.*?/devices')
GPT = 0b00000001
MBR = 0b00000010


def valid_parted_position(pos :str):
    if not len(pos):
        return False

    if pos.isdigit():
        return True

    if pos[-1] == '%' and pos[:-1].isdigit():
        return True

    if pos[-3:].lower() in ['mib', 'kib', 'b', 'tib'] and \
            pos[:-3].replace('.', '', 1).isdigit():
        return True

    if pos[-2:].lower() in ['kb', 'mb', 'gb', 'tb'] and \
            pos[:-2].replace('.', '', 1).isdigit():
        return True

    return False


def valid_fstype(fstype :str) -> bool:
    # https://www.gnu.org/software/parted/manual/html_node/mkpart.html
    # Above link doesn't agree with `man parted` /mkpart documentation:
    """
    file system type can be one of
    'bsd-swap', 'btrfs', 'ext2', 'ext3', 'ext4', 'exfat',
    'f2fs', 'fat12', 'fat16', 'fat32', 'jfs2', 'linux-swap',
    'nilfs2', 'ntfs', 'os2-jfs2', 'reiserfs' and 'xfs'.
    'fat', 'msdos', 'umsdos' and 'vfat' are alias of 'fat32'.
    'jfs' is alias of 'jfs2'.
    'nilfs' is alias of 'nilfs2'.
    """
    return fstype.lower() in [
        # '44bsd', # alias of ffs
        # '5xbsd', # alias of ffs2
        # 'apfs', # not included in `man parted`
                  # (require apfsprogs and fuse)
        'bsd-swap', # not included in `man parted` and
                    # empty file system without signature
        'btrfs',
        'ext2',
        'ext3',
        'ext4',
        'exfat', # not included in `man parted`
        'f2fs', # not included in `man parted`
        'fat', # alias of fat32
        'fat12', # not included in `man parted`
        'fat16',
        'fat32',
        # 'ffs', # not included in `man parted`
                 # (require ufsutils or newfs_ffs)
        # 'ffs2', # not included in `man parted`
                  # (require ufsutils or newfs_ffs)
        # 'hammer', # not included in `man parted`
                    # (require newfs_hammer)
        # 'hammer2', # not included in `man parted`
                     # (require newfs_hammer2)
        # 'hfs', # (require hfsprogs)
        # 'hfs+', # (require hfsprogs)
        # 'hfs+j', # not included in `man parted`
                   # (require hfsprogs)
        # 'hfsx', # not included in `man parted`
                  # (require hfsprogs)
        # 'hfsxj', # not included in `man parted`
                   # (require hfsprogs)
        # 'hpfs', # not included in `man parted`
                  # (require tools)
        # 'iso9660', # not included in `man parted` and
                     # read only filesystem disc
        # 'iso9660joliet', # not included in `man parted` and
                           # read only filesystem disc
        # 'iso9660rock', # not included in `man parted` and
                         # read only filesystem disc
        'jfs', # alias of jfs2
        'jfs2', # not included in `man parted`
        # 'lfs', # not included in `man parted`
                 # (require newfs_lfs)
        'linux-swap',
        'msdos', # alias of fat32
        'nilfs', # alias of nilfs2
        'nilfs2', # not included in `man parted`
        'ntfs',
        'os2-jfs2', # not included in `man parted`
        # 'reiser4', # not included in `man parted`
                     # (require reiser4progs)
        'reiserfs',
        # 'udf', # read only filesystem disc
        # 'ufs', # alias of ffs
        # 'ufs2', # alias of ffs2
        'umsdos', # alias of fat32
        'vfat', # alias of fat32
        'xfs',
        ]


def sort_block_devices_based_on_performance(block_devices):
    result = {device: 0 for device in block_devices}

    for device, weight in result.items():
        if device.spinning:
            weight -= 10
        else:
            weight += 5

        if device.bus_type == 'nvme':
            weight += 20
        elif device.bus_type == 'sata':
            weight += 10

        result[device] = weight

    return result


def filter_disks_below_size_in_gb(devices, gigabytes):
    for disk in devices:
        if disk.size >= gigabytes:
            yield disk


def select_largest_device(devices, gigabytes, filter_out=None):
    if not filter_out:
        filter_out = []

    copy_devices = [*devices]
    for filter_device in filter_out:
        if filter_device in copy_devices:
            copy_devices.pop(copy_devices.index(filter_device))

    copy_devices = \
        list(filter_disks_below_size_in_gb(copy_devices, gigabytes))

    if not len(copy_devices):
        return None

    return max(copy_devices, key=(lambda device: device.size))


def select_disk_larger_than_or_close_to(devices, gigabytes, filter_out=None):
    if not filter_out:
        filter_out = []

    copy_devices = [*devices]
    for filter_device in filter_out:
        if filter_device in copy_devices:
            copy_devices.pop(copy_devices.index(filter_device))

    if not len(copy_devices):
        return None

    return min(
        copy_devices,
        key=(lambda device: abs(device.size - gigabytes)))


def suggest_single_disk_layout(block_device, default_filesystem=None):
    if not default_filesystem:
        from .user_interaction import ask_for_main_filesystem_format
        default_filesystem = ask_for_main_filesystem_format()

    # Minimal size to allow /home partition in GiB
    MINPSZ_HOME = (8, 512)

    layout = {
        block_device.path: {
            'wipe': True,
            'partitions': []
            }
        }

    layout[block_device.path]['partitions'].append(
        {
            # Boot partition
            'type': 'primary',
            'start': '1MiB',
            'size': '512MiB',
            'boot': True,
            'encrypted': False,
            'format': True,
            'mountpoint': '/boot',
            'filesystem': {'type': 'fat32'}
            })

    layout[block_device.path]['partitions'].append(
        {
            # Root partition
            'type': 'primary',
            'start': '513MiB',
            'encrypted': False,
            'format': True,
            'size': '100%'
                if block_device.size < MINPSZ_HOME[0] else (
                    f'{block_device.size/2}GiB'
                    if block_device.size < MINPSZ_HOME[1] else
                    f'{block_device.size/4}GiB'),
            'mountpoint': '/',
            'filesystem': {'type': default_filesystem}
            })

    if block_device.size >= MINPSZ_HOME[0]:
        layout[block_device.path]['partitions'].append(
            {
                # Home partition
                'type': 'primary',
                'encrypted': False,
                'format': True,
                'start': f'{(block_device.size/2)*1024+513}MiB'
                    if block_device.size < MINPSZ_HOME[1] else
                    f'{(block_device.size/4)*1024+513}GiB',
                'size': '100%',
                'mountpoint': '/home',
                'filesystem': {'type': default_filesystem}
                })

    return layout


def suggest_multi_disk_layout(block_devices, default_filesystem=None):
    if not default_filesystem:
        from .user_interaction import ask_for_main_filesystem_format
        default_filesystem = ask_for_main_filesystem_format()

    # Minimal size to allow /home partition in GiB
    MINPSZ_HOME = 8
    # Rough estimate taking in to account user desktops etc (in GiB)
    # TODO: Catch user packages to detect size?
    INSTALLED_SIZE = 4

    block_devices = \
        sort_block_devices_based_on_performance(block_devices).keys()

    home_device = select_largest_device(block_devices, gigabytes=MINPSZ_HOME)
    root_device = select_disk_larger_than_or_close_to(
        block_devices,
        gigabytes=INSTALLED_SIZE,
        filter_out=[home_device])

    log(f'Suggesting multi-disk-layout using {len(block_devices)} disks, '
        f'where {root_device} will be /root and {home_device} will be /home',
        level=logging.DEBUG)

    layout = {
        root_device.path: {
            'wipe': True,
            'partitions': []
            },
        home_device.path: {
            'wipe': True,
            'partitions': []
            }
        }

    layout[root_device.path]['partitions'].append(
        {
            # Boot partition in primary NVM device
            'type': 'primary',
            'start': '1MiB',
            'size': '512MiB',
            'boot': True,
            'encrypted': False,
            'format': True,
            'mountpoint': '/boot',
            'filesystem': {'type': 'fat32'}
            })

    layout[root_device.path]['partitions'].append(
        {
            # Root partition in primary NVM device
            'type': 'primary',
            'start': '513MiB',
            'encrypted': False,
            'format': True,
            'size': '100%',
            'mountpoint': '/',
            'filesystem': {'type': default_filesystem}
            })

    layout[home_device.path]['partitions'].append(
        {
            # Home partition in second NVM device
            'type': 'primary',
            'encrypted': False,
            'format': True,
            'start': '1MiB',
            'size': '100%',
            'mountpoint': '/home',
            'filesystem': {'type': default_filesystem}
            })

    return layout


class BlockDevice:
    def __init__(self, path, info=None):
        if not info:
            # If we don't give any information,
            # we need to auto-fill it.
            # Otherwise any subsequent usage will break.
            info = all_disks()[path].info

        self.path = path
        self.info = info
        self.keep_partitions = True
        self.part_cache = {}
        # TODO: Currently disk encryption is a BIT misleading.
        #       It's actually partition-encryption,
        #       but for future-proofing this
        #       I'm placing the encryption password on a
        #       BlockDevice level.

    def __repr__(self, *args, **kwargs):
        return 'BlockDevice({}, size={}, free_space={}, bus_type={})'.format(
            self.device_or_backfile,
            f'{self.size}GB',
            '+'.join(part[2] for part in self.free_space),
            self.bus_type)

    def __iter__(self):
        for partition in self.partitions:
            yield self.partitions[partition]

    def __getitem__(self, key, *args, **kwargs):
        if key not in self.info:
            raise KeyError(f'{self} does not contain information: "{key}"')

        return self.info[key]

    def __len__(self):
        return len(self.partitions)

    def __lt__(self, left_comparitor):
        return self.path < left_comparitor.path

    def json(self):
        """
        json() has precedence over __dump__, so this is a way
        to give less/partial information for user readability.
        """
        return self.path

    def __dump__(self):
        return {
            self.path: {
                'partuuid': self.uuid,
                'wipe': self.info.get('wipe', None),
                'partitions': [
                    part.__dump__() for part in self.partitions.values()
                    ]
                }
            }

    @property
    def partition_table(self):
        lsblk = SysCommand(f'lsblk -J -o+PTTYPE {self.path}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            return device['pttype']


    @property
    def device_or_backfile(self):
        """
        Returns the actual device-endpoint of the BlockDevice.
        If it's a loop-back-device it returns the back-file,
        For other types it return self.device
        """
        if self.info['type'] == 'loop':
            losetup = SysCommand(['losetup', '--json']).decode('UTF_8')
            for drive in json.loads(losetup)['loopdevices']:
                if not drive['name'] == self.path:
                    continue

                return drive['back-file']
        else:
            return self.device


    @property
    def device(self):
        """
        Returns the device file of the BlockDevice.
        If it's a loop-back-device it returns the /dev/X device,
        If it's a ATA-drive it returns the /dev/X device
        And if it's a crypto-device it returns the parent device
        """
        if 'type' not in self.info:
            raise DiskError(
                f'Could not locate backplane info for "{self.path}"')

        if self.info['type'] in ['disk','loop']:
            return self.path
        elif self.info['type'][:4] == 'raid':
            # This should catch /dev/md## raid devices

            return self.path
        elif self.info['type'] == 'crypt':
            if 'pkname' not in self.info:
                raise DiskError(f'A crypt device ({self.path}) without a '
                                'parent kernel device name.')

            return f"/dev/{self.info['pkname']}"
        else:
            log(f'Unknown blockdevice type for {self.path}: '
                "{self.info['type']}",
                level=logging.DEBUG)

        #if not stat.S_ISBLK(os.stat(full_path).st_mode):
        #    raise DiskError(
        #        f'Selected disk "{full_path}" is not a block device.')


    @property
    def partitions(self):
        SysCommand(['partprobe', self.path])
        result = SysCommand(['lsblk', '-J', self.path])
        if b'not a block device' in result:
            raise DiskError('Can not read partitions off something that is '
                            f'not a block device: {self.path}')

        if not result[:1] == b'{':
            raise DiskError(
                f'Error getting JSON output from: lsblk -J {self.path}')

        r = json.loads(result.decode('UTF-8'))
        if len(r['blockdevices']) and 'children' in r['blockdevices'][0]:
            root_path = f"/dev/{r['blockdevices'][0]['name']}"
            for part in r['blockdevices'][0]['children']:
                part_id = part['name'][len(os.path.basename(self.path)):]
                if part_id not in self.part_cache:
                    # TODO: Force over-write even if in cache?
                    if part_id not in self.part_cache or \
                            self.part_cache[part_id].size != part['size']:
                        self.part_cache[part_id] = Partition(
                            root_path + part_id,
                            self,
                            part_id=part_id,
                            size=part['size'])

        return {k: self.part_cache[k] for k in sorted(self.part_cache)}

    @property
    def partition(self):
        all_partitions = self.partitions
        return [all_partitions[k] for k in all_partitions]

    @property
    def uuid(self):
        log('BlockDevice().uuid is untested!',
            level=logging.WARNING,
            fg='yellow')
        """
        Returns the disk UUID as returned by lsblk.
        This is more reliable than relying on /dev/disk/by-partuuid as
        it doesn't seam to be able to detect md raid partitions.
        """
        lsblk = SysCommand(f'lsblk -J -o+UUID {self.path}').decode('UTF-8')
        for partition in json.loads(lsblk)['blockdevices']:
            return partition.get('uuid', None)

    def convert_size_to_gb(self, size):
        units = {
            'P': lambda s: float(s) * 2048,
            'T': lambda s: float(s) * 1024,
            'G': lambda s: float(s),
            'M': lambda s: float(s) / 1024,
            'K': lambda s: float(s) / 2048,
            'B': lambda s: float(s) / 3072,
            }
        unit = size[-1]
        return float(units.get(unit, lambda s: None)(size[:-1]))

    @property
    def size(self):
        lsblk = SysCommand(f'lsblk -J -o+SIZE {self.path}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            return self.convert_size_to_gb(device['size'])

    @property
    def bus_type(self):
        lsblk = \
            SysCommand(f'lsblk -J -o+ROTA,TRAN {self.path}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            return device['tran']

    @property
    def spinning(self):
        lsblk = \
            SysCommand(f'lsblk -J -o+ROTA,TRAN {self.path}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            return device['rota'] is True

    @property
    def free_space(self):
        # NOTE: parted -s will default to `cancel` on prompt,
        # skipping any partition that is "outside" the disk.
        # in /dev/sr0 this is usually the case with HyperISO,
        # so the free will ignore the ESP partition and
        # just give the "free" space.
        # Doesn't harm us,
        # but worth noting in case something weird happens.
        for line in SysCommand(f'parted -sm {self.path} print free'):
            free_space = line.decode('UTF-8')
            if 'free' in free_space:
                _, start, end, size, *_ = free_space.strip('\r\n;').split(':')
                yield (start, end, size)
            elif 'unknown' in free_space:
                start = '0.00B'
                _, end, *_ = free_space.strip('\r\n;').split(':')
                size = end
                yield (start, end, size)

    @property
    def largest_free_space(self):
        info = None
        for space_info in self.free_space:
            if not info:
                info = space_info
            else:
                # [-1] = size
                if space_info[-1] > info[-1]:
                    info = space_info

        return info

    def has_partitions(self):
        return len(self.partitions)

    def has_mount_point(self, mountpoint):
        for partition in self.partitions:
            if self.partitions[partition].mountpoint == mountpoint:
                return True

        return False

    def flush_cache(self):
        self.part_cache = {}

    def get_partition(self, uuid):
        for partition in self:
            if partition.uuid == uuid:
                return partition


class Partition:
    def __init__(
            self,
            path: str,
            block_device: BlockDevice,
            part_id=None,
            size=-1,
            filesystem=None,
            mountpoint=None,
            encrypted=False,
            autodetect_filesystem=True):
        if not part_id:
            part_id = os.path.basename(path)

        self.block_device = block_device
        self.path = path
        self.part_id = part_id
        self.mountpoint = mountpoint
        self.target_mountpoint = mountpoint
        self.filesystem = filesystem
        self.size = size  # TODO: Refresh?
        self._encrypted = None
        self.encrypted = encrypted
        self.allow_formatting = False
        if mountpoint:
            self.mount(mountpoint)

        mount_information = get_mount_info(self.path)
        if self.mountpoint != mount_information.get('target', None) and \
                mountpoint:
            raise DiskError(
                f'{self} was given a mountpoint but the actual mountpoint '
                f"differs: {mount_information.get('target', None)}")

        if target := mount_information.get('target', None):
            self.mountpoint = target

        if not self.filesystem and autodetect_filesystem:
            fstype = \
                mount_information.get('fstype', get_filesystem_type(path))
            if fstype:
                self.filesystem = fstype

        if self.filesystem == 'crypto_LUKS':
            self.encrypted = True

    def __lt__(self, left_comparitor):
        if type(left_comparitor) == Partition:
            left_comparitor = left_comparitor.path
        else:
            left_comparitor = str(left_comparitor)

        # Not quite sure the order here is correct.
        # But /dev/nvme0n1p1 comes before /dev/nvme0n1p5 so
        # seems correct.
        return self.path < left_comparitor

    def __repr__(self, *args, **kwargs):
        mount_repr = ''
        if self.mountpoint:
            mount_repr = f', mounted={self.mountpoint}'
        elif self.target_mountpoint:
            mount_repr = f', rel_mountpoint={self.target_mountpoint}'

        if self._encrypted:
            return 'Partition(path={}, size={}, {}, parent={}, fs={})'.format(
                self.path,
                self.size,
                f'PARTUUID={self.uuid}',
                self.real_device,
                f'{self.filesystem}{mount_repr}')
        else:
            return 'Partition(path={}, size={}, PARTUUID={}, fs={})'.format(
                self.path,
                self.size,
                self.uuid,
                f'{self.filesystem}{mount_repr}')

    def __dump__(self):
        return {
            'type': 'primary',
            'PARTUUID': self.uuid,
            'format': self.allow_formatting,
            'boot': self.boot,
            # Unused ESP is temporarily commented.
            # 'ESP': self.boot,
            'mountpoint': self.target_mountpoint,
            'encrypted': self._encrypted,
            'start': self.start,
            'size': self.end,
            'filesystem': {'type': get_filesystem_type(self.path)}
            }

    @property
    def sector_size(self):
        lsblk = SysCommand(f'lsblk -J -o+LOG-SEC {self.path}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            return device.get('log-sec', None)

    @property
    def start(self):
        sfdisk = \
            SysCommand(f'sfdisk -J {self.block_device.path}').decode('UTF-8')
        output = json.loads(sfdisk)
        for partition in \
                output.get('partitiontable', {}).get('partitions', []):
            if partition['node'] == self.path:
                return partition['start']# * self.sector_size

    @property
    def end(self):
        # TODO: Verify that the logic holds up,
        #       that 'size' is the size without 'start' added to it.
        sfdisk = \
            SysCommand(f'sfdisk -J {self.block_device.path}').decode('UTF-8')
        output = json.loads(sfdisk)
        for partition in \
                output.get('partitiontable', {}).get('partitions', []):
            if partition['node'] == self.path:
                return partition['size']# * self.sector_size

    @property
    def boot(self):
        sfdisk = \
            SysCommand(f'sfdisk -J {self.block_device.path}').decode('UTF-8')
        output = json.loads(sfdisk)
        # Get the bootable flag from the sfdisk output:
        # {
        #     'partitiontable': {
        #         'label': 'dos',
        #         'id': '0xd202c10a',
        #         'device': '/dev/loop0',
        #         'unit': 'sectors',
        #         'sectorsize': 512,
        #         'partitions': [
        #             {
        #                 'node': '/dev/loop0p1',
        #                 'start': 2048,
        #                 'size': 10483712,
        #                 'type': '83',
        #                 'bootable': true
        #                 }
        #             ]
        #         }
        #     }
        for partition in \
                output.get('partitiontable', {}).get('partitions', []):
            if partition['node'] == self.path:
                return partition.get('bootable', False)

        return False

    @property
    def partition_table(self):
        lsblk = SysCommand(f'lsblk -J -o+PTTYPE {self.path}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            return device['pttype']

    @property
    def uuid(self) -> Optional[str]:
        """
        Returns the PARTUUID as returned by lsblk.
        This is more reliable than relying on /dev/disk/by-partuuid as
        it does not seam to be able to detect md raid partitions.
        """
        lsblk = \
            SysCommand(f'lsblk -J -o+PARTUUID {self.path}').decode('UTF-8')
        for partition in json.loads(lsblk)['blockdevices']:
            return partition.get('partuuid', None)

        return None

    @property
    def encrypted(self):
        return self._encrypted

    @encrypted.setter
    def encrypted(self, value: bool):
        self._encrypted = value

    @property
    def parent(self):
        return self.real_device

    @property
    def real_device(self):
        lsblk = SysCommand('lsblk -J').decode('UTF-8')
        for blockdevice in json.loads(lsblk)['blockdevices']:
            parent = \
                self.find_parent_of(blockdevice, os.path.basename(self.path))
            if parent:
                return f'/dev/{parent}'
            #raise DiskError('Could not find appropriate parent for '
            #                f'encrypted partition {self}')

        return self.path

    def detect_inner_filesystem(self, password):
        log(f'Trying to detect inner filesystem format on {self} '
            '(This might take a while)',
            level=logging.INFO)
        from .luks import luks2
        try:
            with luks2(self,
                       storage.get('ENC_IDENTIFIER', 'ai')+'loop',
                       password,
                       auto_unmount=True) as unlocked_device:
                return unlocked_device.filesystem

        except SysCallError:
            return None

    def has_content(self):
        fstype = get_filesystem_type(self.path)
        if not fstype or 'swap' in fstype:
            return False

        temporary_mountpoint = '/tmp/' + hashlib.md5(
            bytes(f'{time.time()}', 'UTF-8') + os.urandom(12)).hexdigest()
        temporary_path = pathlib.Path(temporary_mountpoint)
        temporary_path.mkdir(parents=True, exist_ok=True)
        mount = SysCommand(f'mount {self.path} {temporary_mountpoint}')
        if mount.exit_code != 0:
            raise DiskError('Could not mount and check for content on '
                            f"{self.path} because: {b''.join(mount)}")

        files = len(glob.glob(f'{temporary_mountpoint}/*'))
        iterations = 0
        umount = SysCommand(f'umount -R {temporary_mountpoint}')
        while umount.exit_code != 0 and (iterations := iterations + 1) < 10:
            time.sleep(1)

        temporary_path.rmdir()
        return True if files > 0 else False

    def encrypt(self, *args, **kwargs):
        """
        A wrapper function for luks2() instances and
        the .encrypt() method of that instance.
        """
        from .luks import luks2
        handle = luks2(self, None, None)
        return handle.encrypt(self, *args, **kwargs)

    def format(self, filesystem=None, path=None, log_formatting=True):
        """
        Format can be given an overriding path,
        for instance /dev/null to test
        the formatting functionality and
        in essence the support for the given filesystem.
        """
        if filesystem is None:
            filesystem = self.filesystem

        if path is None:
            path = self.path

        # To avoid "unable to open /dev/x: No such file or directory"
        start_wait = time.time()
        while pathlib.Path(path).exists() is False and \
                time.time() - start_wait < 10:
            time.sleep(0.025)

        if log_formatting:
            log(f'Formatting {path} -> {filesystem}', level=logging.INFO)

        #if filesystem == 'apfs':
        #    mkfs = SysCommand(f'mkfs.apfs {path}')
        #    if mkfs.exit_code != 0:
        #        raise DiskError(f'Could not format {path} with '
        #                        f'{filesystem} because: {mkfs}')
        #
        #    self.filesystem = filesystem
        if filesystem == 'bsd-swap':
            # bsd-swap is an empty file system and not have a signature
            mkfs = SysCommand(f'dd bs=1M if=/dev/zero of={path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'btrfs':
            mkfs = SysCommand(f'mkfs.btrfs -f {path}').decode('UTF-8')
            if 'UUID:' not in mkfs:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'ext2':
            mkfs = SysCommand(f'mke2fs -t ext2 -F {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f"{filesystem} because: {b''.join(mkfs)}")

            self.filesystem = filesystem
        elif filesystem == 'ext3':
            mkfs = SysCommand(f'mke2fs -j -t ext3 -F {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f"{filesystem} because: {b''.join(mkfs)}")

            self.filesystem = filesystem
        elif filesystem == 'ext4':
            mkfs = SysCommand(f'mke2fs -j -t ext4 -T ext4 -F {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: '
                                f"{mkfs.decode('UTF-8')}")

            self.filesystem = filesystem
        elif filesystem == 'exfat':
            mkfs = SysCommand(f'mkfs.exfat {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'f2fs':
            mkfs = SysCommand(f'mkfs.f2fs -f {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: '
                                f"{mkfs.decode('UTF-8')}")

            self.filesystem = filesystem
        elif filesystem == 'fat12':
            mkfs = SysCommand(f'mkdosfs -F 12 {path}').decode('UTF-8')
            if ('mkdosfs' not in mkfs and 'mkfs.fat' not in mkfs) or \
                    'command not found' in mkfs:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'fat16':
            mkfs = SysCommand(f'mkdosfs -F 16 {path}').decode('UTF-8')
            if ('mkdosfs' not in mkfs and 'mkfs.fat' not in mkfs) or \
                    'command not found' in mkfs:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'fat32' \
                or filesystem == 'fat' \
                or filesystem == 'msdos' \
                or filesystem == 'umsdos' \
                or filesystem == 'vfat':
            mkfs = SysCommand(f'mkdosfs -F 32 {path}').decode('UTF-8')
            if ('mkdosfs' not in mkfs and 'mkfs.fat' not in mkfs) or \
                    'command not found' in mkfs:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'fat32'
        elif filesystem == 'ffs' \
                or filesystem == '44bsd' \
                or filesystem == 'ufs':
            mkfs = SysCommand(f'mkfs.ufs -O 1 {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'ffs'
        elif filesystem == 'ffs2' \
                or filesystem == '5xbsd' \
                or filesystem == 'ufs2':
            mkfs = SysCommand(f'mkfs.ufs -O 2 {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'ffs2'
        elif filesystem == 'hfs':
            mkfs = SysCommand(f'mkfs.hfs -h {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'hfs+':
            mkfs = SysCommand(f'mkfs.hfs {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'hfs+j':
            mkfs = SysCommand(f'mkfs.hfs -J {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'hfs+'
        elif filesystem == 'hfsx':
            mkfs = SysCommand(f'mkfs.hfs -s {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'hfsxj':
            mkfs = SysCommand(f'mkfs.hfs -sJ {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'hfsx'
        elif filesystem == 'jfs2' \
                or filesystem == 'jfs':
            mkfs = SysCommand(f'jfs_mkfs {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'jfs2'
        elif filesystem == 'linux-swap':
            mkfs = SysCommand(f'mkswap -f {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'ntfs':
            mkfs = SysCommand(f'mkntfs {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'nilfs2' \
                or filesystem == 'nilfs':
            mkfs = SysCommand(f'mkfs.nilfs2 {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'nilfs2'
        elif filesystem == 'os2-jfs2':
            mkfs = SysCommand(f'jfs_mkfs -O {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = 'jfs2'
        elif filesystem == 'reiser4':
            mkfs = SysCommand(f'mkreiser4 {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'reiserfs':
            mkfs = SysCommand(f'mkreiserfs {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: {mkfs}')

            self.filesystem = filesystem
        elif filesystem == 'xfs':
            mkfs = SysCommand(f'mkfs.xfs -f {path}')
            if mkfs.exit_code != 0:
                raise DiskError(f'Could not format {path} with '
                                f'{filesystem} because: '
                                f"{mkfs.decode('UTF-8')}")

            self.filesystem = filesystem
        elif filesystem == 'crypto_LUKS':
            # from .luks import luks2
            # encrypted_partition = luks2(self, None, None)
            # encrypted_partition.format(path)
            self.filesystem = filesystem
        else:
            raise UnknownFilesystemFormat(
                f"Fileformat '{filesystem}' is not yet implemented.")

        if get_filesystem_type(path) == 'crypto_LUKS' or \
                get_filesystem_type(self.real_device) == 'crypto_LUKS':
            self.encrypted = True
        else:
            self.encrypted = False

        return True

    def find_parent_of(self, data, name, parent=None):
        if data['name'] == name:
            return parent
        elif 'children' in data:
            for child in data['children']:
                if parent := \
                        self.find_parent_of(child, name, parent=data['name']):
                    return parent

    def mount(self, target, fs=None, options=''):
        if not self.mountpoint:
            log(f'Mounting {self} to {target}', level=logging.INFO)
            if not fs:
                if not self.filesystem:
                    raise DiskError(
                        'Need to format (or define) the filesystem on '
                        f'{self} before mounting.')

                fs = self.filesystem

            pathlib.Path(target).mkdir(parents=True, exist_ok=True)
            try:
                if options:
                    SysCommand(f'mount -o {options} {self.path} {target}')
                else:
                    SysCommand(f'mount {self.path} {target}')

            except SysCallError as err:
                raise err

            self.mountpoint = target
            return True

    def unmount(self):
        try:
            SysCommand(f'umount {self.path}')
        except SysCallError as err:
            exit_code = err.exit_code
            # Without to much research,
            # it seams that low error codes are errors.
            # And above 8k is indicators such as "/dev/x not mounted.".
            # So anything in between 0 and 8k are errors (?).
            if 0 < exit_code < 8000:
                raise err

        self.mountpoint = None
        return True

    def umount(self):
        return self.unmount()

    def filesystem_supported(self):
        """
        The support for a filesystem (this partition) is
        tested by calling partition.format() with a path set to
        '/dev/null' which returns two exceptions:
        1. SysCallError saying that /dev/null is not formattable
           - but the filesystem is supported
        2. UnknownFilesystemFormat that indicates that we
           do not support the given filesystem type
        """
        try:
            self.format(self.filesystem,
                        '/dev/null',
                        log_formatting=False,
                        allow_formatting=True)
        except (SysCallError, DiskError):
            # We supported it, but /dev/null is not formatable as
            # expected so the mkfs call exited with an error code
            pass
        except UnknownFilesystemFormat as err:
            raise err

        return True


class Filesystem:
    # TODO:
    #   When instance of a HDD is selected, check all usages and
    #   gracefully unmount them as well as close any crypto handles.
    def __init__(self, blockdevice, mode):
        self.blockdevice = blockdevice
        self.mode = mode

    def __enter__(self, *args, **kwargs):
        if self.blockdevice.keep_partitions is False:
            log(f'Wiping {self.blockdevice} by using {self.mode} '
                'partition table',
                level=logging.DEBUG)
            if self.mode == GPT:
                if self.parted_mklabel(self.blockdevice.device, 'gpt'):
                    self.blockdevice.flush_cache()
                    return self
                else:
                    raise DiskError(
                        'Problem setting the disk label type to GPT:',
                        f'parted -s {self.blockdevice.device} mklabel gpt')

            elif self.mode == MBR:
                if self.parted_mklabel(self.blockdevice.device, 'msdos'):
                    return self
                else:
                    raise DiskError(
                        'Problem setting the disk label type to msdos:',
                        f'parted -s {self.blockdevice.device} mklabel msdos')
            else:
                raise DiskError(
                    f'Unknown mode selected to format in: {self.mode}')

        return self

    def __repr__(self):
        return f'Filesystem(blockdevice={self.blockdevice}, mode={self.mode})'

    def __exit__(self, *args, **kwargs):
        # TODO:
        #    https://stackoverflow.com/questions/28157929/
        #    how-to-safely-handle-an-exception-inside-a-context-manager
        if len(args) >= 2 and args[1]:
            raise args[1]

        SysCommand('sync')
        return True

    def partuuid_to_index(self, uuid):
        lsblk = SysCommand('lsblk -J -o+PARTUUID '
                           f'{self.blockdevice.device}').decode('UTF-8')
        for device in json.loads(lsblk)['blockdevices']:
            for index, partition in enumerate(device['children']):
                if partition['partuuid'].lower() == uuid:
                    return index

    def load_layout(self, layout :dict):
        from .luks import luks2

        # If the layout tells us to wipe the drive, we do so
        if layout.get('wipe', False):
            if self.mode == GPT:
                if not self.parted_mklabel(self.blockdevice.device, 'gpt'):
                    raise KeyError(f'Could not create a GPT label on {self}')

            elif self.mode == MBR:
                if not self.parted_mklabel(self.blockdevice.device, 'msdos'):
                    raise KeyError('Could not create a MSDOS label on '
                                   f'{self}')

        # We then iterate the partitions in order
        for partition in layout.get('partitions', []):
            # We don't want to re-add an existing partition
            # (those containing a UUID already)
            if partition.get('format', False) and \
                    not partition.get('PARTUUID', None):
                print('Adding partition....')
                partition['device_instance'] = self.add_partition(
                    partition.get('type', 'primary'),
                    # TODO: Revisit sane block starts
                    #       (4MB for memorycards for instance)
                    start=partition.get('start', '1MiB'),
                    end=partition.get('size', '100%'),
                    fstype=partition.get('filesystem', {}).get('type',
                                                               'btrfs'))
                # TODO: device_instance some times become None
                #       print('Device instance:',
                #             partition['device_instance'])

            elif (partition_uuid := partition.get('PARTUUID')) and \
                    (partition_instance :=
                        self.blockdevice.get_partition(uuid=partition_uuid)):
                print('Re-using partition_instance:', partition_instance)
                partition['device_instance'] = partition_instance
            else:
                raise ValueError(
                    f'{self}.load_layout() does not know how to '
                    'continue without a new partition definition or a '
                    f"UUID ({partition.get('PARTUUID')}) on the device "
                    '({}).'.format(
                        self.blockdevice.get_partition(uuid=partition_uuid)))

            if partition.get('filesystem', {}).get('type', False):
                if partition.get('encrypted', False):
                    if not partition.get('password'):
                        if storage['arguments'] == 'silent':
                            raise ValueError(
                                'Missing encryption password for '
                                f"{partition['device_instance']}")
                        else:
                            from .user_interaction import get_password
                            partition['password'] = get_password(
                                'Enter a encryption password for '
                                f"{partition['device_instance']}")

                    partition['device_instance'].encrypt(
                        password=partition['password'])
                    with luks2(partition['device_instance'],
                               storage.get('ENC_IDENTIFIER', 'ai')+'loop',
                               partition['password']) as unlocked_device:
                        if not partition.get('format'):
                            if storage['arguments'] == 'silent':
                                raise ValueError(
                                    'Missing file system type to format on '
                                    'newly created encrypted partition '
                                    f"{partition['device_instance']}")
                            else:
                                if not partition.get('filesystem'):
                                    partition['filesystem'] = {}

                                if not partition['filesystem'].get('type',
                                                                   False):
                                    while True:
                                        partition['filesystem']['type'] = \
                                            input('Enter a valid file system '
                                                  'type for newly encrypted '
                                                  'partition {}: '.format(
                                                      partition.get(
                                                          'filesystem').get(
                                                              'type')))
                                        if not partition.get(
                                                'filesystem').get('type') \
                                                    or valid_fstype(
                                                        partition.get(
                                                            'filesystem').get(
                                                                'type')) \
                                                                    is False:
                                            print('You need to enter a valid '
                                                  'file system type in order '
                                                  'to continue. '
                                                  'See `man parted` for '
                                                  "valid file system type's.")
                                            continue

                                        break

                        unlocked_device.format(
                            partition['filesystem']['type'])
                elif partition.get('format', False):
                    partition['device_instance'].format(
                        partition['filesystem']['type'])

            if partition.get('boot', False):
                self.set(
                    self.partuuid_to_index(
                        partition['device_instance'].uuid),
                        'boot on')

    def find_partition(self, mountpoint):
        for partition in self.blockdevice:
            if partition.target_mountpoint == mountpoint or \
                    partition.mountpoint == mountpoint:
                return partition

    def raw_parted(self, string: str):
        if (parted := SysCommand(f'parted -s {string}')).exit_code != 0:
            log(f'Parted ended with a bad exit code: {parted}',
                level=logging.ERROR,
                fg='red')

        return parted

    def parted(self, string: str):
        """
        Performs a parted execution of the given string

        :param string: A raw string passed to parted -s <string>
        :type string: str
        """
        return self.raw_parted(string).exit_code == 0

    def use_entire_disk(self, root_filesystem_type='ext4') -> Partition:
        # TODO: Implement this with declarative profiles instead.
        raise ValueError(
            'Installation().use_entire_disk() has to be re-worked.')

    def add_partition(self,
                      type,
                      start,
                      end,
                      fstype=None):
        log(f'Adding partition to {self.blockdevice}, {start}->{end}',
            level=logging.INFO)
        previous_partition_uuids = {
            partition.uuid for partition in
            self.blockdevice.partitions.values()
            }
        if self.mode == MBR:
            if len(self.blockdevice.partitions) > 3:
                DiskError('Too many partitions on disk, '
                          'MBR disks can only have 3 parimary partitions')

        if fstype:
            parted_string = '{} mkpart {}'.format(
                self.blockdevice.device,
                f'{type} {fstype} {start} {end}')
        else:
            parted_string = '{} mkpart {}'.format(
                self.blockdevice.device,
                f'{type} {start} {end}')

        if self.parted(parted_string):
            start_wait = time.time()
            while previous_partition_uuids == {
                    partition.uuid for partition in
                    self.blockdevice.partitions.values()
                    }:
                if time.time() - start_wait > 10:
                    raise DiskError(
                        'New partition never showed up after adding new '
                        'partition on {self} (timeout 10 seconds).')

                time.sleep(0.025)

            # Todo: Find a better way to detect if
            #       the new UUID of the partition has showed up.
            #       But this will address (among other issues)
            # Let the kernel catch up with quick block devices
            # (nvme for instance)
            time.sleep(float(storage['arguments'].get('disk-sleep', 2.0)))
            return self.blockdevice.get_partition(
                uuid=(
                    previous_partition_uuids ^ {
                        partition.uuid for partition in
                        self.blockdevice.partitions.values()
                        }
                    ).pop())

    def set_name(self, partition: int, name: str):
        return self.parted(
            f'{self.blockdevice.device} name {partition + 1} "{name}"') == 0

    def set(self, partition: int, string: str):
        log(f'Setting {string} on (parted) partition index {partition+1}',
            level=logging.INFO)
        return self.parted(
            f'{self.blockdevice.device} set {partition + 1} {string}') == 0

    def parted_mklabel(self, device: str, disk_label: str):
        log(f'Creating a new partition labling on {device}',
            level=logging.INFO,
            fg='yellow')
        # Try to unmount devices before attempting to run mklabel
        try:
            SysCommand(f'bash -c "umount {device}?"')
        except:
            pass

        return \
            self.raw_parted(f'{device} mklabel {disk_label}').exit_code == 0


def device_state(name, *args, **kwargs):
    # Based out of:
    # https://askubuntu.com/questions/528690/
    # how-to-get-list-of-all-non-removable-disk-device-names-ssd-hdd-
    # and-sata-ide-onl/528709#528709
    if os.path.isfile(f'/sys/block/{name}/device/block/{name}/removable'):
        with open(f'/sys/block/{name}/device/block/{name}/removable') as f:
            if f.read(1) == '1':
                return

    path = ROOT_DIR_PATTERN.sub('', os.readlink(f'/sys/block/{name}'))
    hotplug_buses = ('usb', 'ieee1394', 'mmc', 'pcmcia', 'firewire')
    for bus in hotplug_buses:
        if os.path.exists(f'/sys/bus/{bus}'):
            for device_bus in os.listdir(f'/sys/bus/{bus}/devices'):
                device_link = ROOT_DIR_PATTERN.sub(
                    '',
                    os.readlink(f'/sys/bus/{bus}/devices/{device_bus}'))
                if re.search(device_link, path):
                    return

    return True


# lsblk --json -l -n -o path
def all_disks(*args, **kwargs):
    kwargs.setdefault('partitions', False)
    drives = {}

    lsblk = \
        SysCommand('lsblk --json -l -n -o {},{},{},{},{},{},{}'.format(
            'path',
            'size',
            'type',
            'mountpoint',
            'label',
            'pkname',
            'model')).decode('UTF_8')
    for drive in json.loads(lsblk)['blockdevices']:
        if not kwargs['partitions'] and drive['type'] == 'part':
            continue

        drives[drive['path']] = BlockDevice(drive['path'], drive)

    return drives


def convert_to_gigabytes(string):
    unit = string.strip()[-1]
    size = float(string.strip()[:-1])
    if unit == 'M':
        size = size / 1024
    elif unit == 'T':
        size = size * 1024

    return size


def harddrive(size=None, model=None, fuzzy=False):
    collection = all_disks()
    for drive in collection:
        if size and convert_to_gigabytes(collection[drive]['size']) != size:
            continue
        if model and (
                collection[drive]['model'] is None or
                collection[drive]['model'].lower() != model.lower()
                ):
            continue

        return collection[drive]


def get_mount_info(path) -> dict:
    try:
        output = SysCommand(f'findmnt --json {path}').decode('UTF-8')
    except SysCallError:
        return {}

    if not output:
        return {}

    output = json.loads(output)
    if 'filesystems' in output:
        if len(output['filesystems']) > 1:
            raise DiskError(f"Path '{path}' contains multiple mountpoints: "
                            f"{output['filesystems']}")

        return output['filesystems'][0]


def get_partitions_in_use(mountpoint) -> list:
    try:
        output = SysCommand(f'findmnt --json -R {mountpoint}').decode('UTF-8')
    except SysCallError:
        return []

    mounts = []
    if not output:
        return []

    output = json.loads(output)
    for target in output.get('filesystems', []):
        mounts.append(
            Partition(target['source'],
                      None,
                      filesystem=target.get('fstype', None),
                      mountpoint=target['target']))
        for child in target.get('children', []):
            mounts.append(
                Partition(child['source'],
                          None,
                          filesystem=child.get('fstype', None),
                          mountpoint=child['target']))

    return mounts


def get_filesystem_type(path):
    try:
        return SysCommand(
            f'blkid -o value -s TYPE {path}').decode('UTF-8').strip()
    except SysCallError:
        return None


def disk_layouts():
    try:
        return json.loads(
            SysCommand('lsblk -f -o+TYPE,SIZE -J').decode('UTF-8'))
    except SysCallError as err:
        log(f'Could not return disk layouts: {err}')
        return None


def encrypted_partitions(blockdevices :dict) -> bool:
    for partition in blockdevices.values():
        if partition.get('encrypted', False):
            yield partition

def find_partition_by_mountpoint(block_devices, relative_mountpoint :str):
    for device in block_devices:
        for partition in block_devices[device]['partitions']:
            if partition.get('mountpoint', None) == relative_mountpoint:
                return partition
