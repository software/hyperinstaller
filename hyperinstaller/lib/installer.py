from .disk import *
from .hardware import *
from .locale_helpers import (
    verify_cui_keyboard_layout,
    verify_gui_keyboard_layout
    )
from .mirrors import *
from .plugins import plugins
from .storage import storage
from .user_interaction import *

# Any package that the Installer() is responsible for
# (optional and the default ones)
__packages__ = ['base', 'kernel-firmware']


class Installer:
    """
    `Installer()` is the wrapper for most basic installation steps.
    It also wraps :py:func:`~hyperinstaller.Installer.pacstrap` among
    other things.

    :param partition: Requires a partition as the first argument,
        this is so that the installer can mount to `mountpoint` and
        strap packages there.
    :type partition: class:`hyperinstaller.Partition`

    :param boot_partition: There's two reasons for needing a
        boot partition argument, The first being so that `mkinitcpio`
        can place the `vmlinuz-linux-libre-lts` kernel at the
        right place during the `pacstrap` or `linux-libre-lts` and
        the base packages for a minimal installation.
        The second being when
        :py:func:`~hyperinstaller.Installer.add_bootloader` is called,
        A `boot_partition` must be known to the installer before this
        is called.
    :type boot_partition: class:`hyperinstaller.Partition`

    :param profile: A profile to install, this is optional and
        can be called later manually.
        This just simplifies the process by not having to call
        :py:func:`~hyperinstaller.Installer.install_profile` later on.
    :type profile: str, optional

    :param hostname: The given /etc/hostname for the machine.
    :type hostname: str, optional
    """
    def __init__(self, target, *, base_packages=None, kernels=None):
        if base_packages is None:
            base_packages = __packages__[:3]

        if kernels is None:
            kernels = ['linux-libre-lts']

        self.kernels = kernels
        self.target = target
        self.init_time = time.strftime('%Y-%m-%d_%H-%M-%S')
        self.milliseconds = int(str(time.time()).split('.')[1])

        self.helper_flags = {'base': False, 'bootloader': False}

        self.base_packages = base_packages.split(' ') \
            if type(base_packages) is str else base_packages
        for kernel in kernels:
            self.base_packages.append(kernel)

        self.post_base_install = []

        storage['session'] = self

        self.MODULES = []
        self.BINARIES = []
        self.FILES = []
        self.HOOKS = [
            'base',
            'udev',
            'autodetect',
            'keyboard',
            'keymap',
            'modconf',
            'block',
            'filesystems',
            'fsck'
            ]
        self.KERNEL_PARAMS = []

    def log(self, *args, level=logging.DEBUG, **kwargs):
        """
        installer.log() wraps output.log() mainly to set a
        default log-level for this install session.
        Any manual override can be done per log() call.
        """
        log(*args, level=level, **kwargs)

    def __enter__(self, *args, **kwargs):
        return self

    def __exit__(self, *args, **kwargs):
        # b''.join(sys_command('sync'))
        # No need to, since the underlying fs() object will call sync.
        # TODO: https://stackoverflow.com/questions/28157929/how-to-
        #       safely-handle-an-exception-inside-a-context-manager

        if len(args) >= 2 and args[1]:
            self.log(args[1], level=logging.ERROR, fg='red')

            self.sync_log_to_install_medium()

            # We avoid printing /mnt/<log path> because that might
            # confuse people if they note it down and then reboot,
            # and a identical log file will be found in the live medium
            # anyway.
            print('[!] A log file has been created here: '
                  f"{os.path.join(storage['LOG_PATH'], storage['LOG_FILE'])}"
                  '\n    Please submit this issue (and file) to '
                  'https://issues.hyperbola.info')
            raise args[1]

        self.genfstab()

        if not (missing_steps := self.post_install_check()):
            self.log('Installation completed without any errors. '
                     'You may now reboot.',
                     fg='green',
                     level=logging.INFO)
            self.sync_log_to_install_medium()

            return True
        else:
            self.log('Some required steps were not successfully '
                     'installed/configured before leaving the installer:',
                     fg='red',
                     level=logging.WARNING)
            for step in missing_steps:
                self.log(f' - {step}', fg='red', level=logging.WARNING)

            self.log('Detailed error logs can be found at: '
                     f"{storage['LOG_PATH']}\n"
                     'Submit this zip file as an issue to '
                     'https://issues.hyperbola.info',
                     level=logging.WARNING)

            self.sync_log_to_install_medium()
            return False

    @property
    def partitions(self):
        return get_partitions_in_use(self.target)

    def sync_log_to_install_medium(self):
        # Copy over the install log (if there is one) to the
        # install medium if at least the base has been strapped in,
        # otherwise we won't have a filesystem/structure to copy to.
        if self.helper_flags.get('base-strapped', False) is True:
            if filename := storage.get('LOG_FILE', None):
                absolute_logfile = \
                    os.path.join(storage.get('LOG_PATH', './'), filename)

                os_path = f'{self.target}/{os.path.dirname(absolute_logfile)}'
                if not os.path.isdir(os_path):
                    os.makedirs(os_path)

                shutil.copy2(absolute_logfile,
                             f'{self.target}/{absolute_logfile}')

        return True

    def mount_ordered_layout(self, layouts :dict):
        from .luks import luks2

        mountpoints = {}
        for blockdevice in layouts:
            for partition in layouts[blockdevice]['partitions']:
                mountpoints[partition['mountpoint']] = partition

        for mountpoint in sorted(mountpoints.keys()):
            if mountpoints[mountpoint]['encrypted']:
                loopdev = storage.get('ENC_IDENTIFIER', 'ai')+'loop'
                password = mountpoints[mountpoint]['password']
                with luks2(mountpoints[mountpoint]['device_instance'],
                           loopdev,
                           password,
                           auto_unmount=False) as unlocked_device:
                    unlocked_device.mount(f'{self.target}{mountpoint}')
            else:
                mountpoints[mountpoint]['device_instance'].mount(
                    f'{self.target}{mountpoint}')

    def mount(self, partition, mountpoint, create_mountpoint=True):
        if create_mountpoint and not os.path.isdir(
                f'{self.target}{mountpoint}'):
            os.makedirs(f'{self.target}{mountpoint}')

        partition.mount(f'{self.target}{mountpoint}')

    def post_install_check(self, *args, **kwargs):
        return [
            step for step, flag in self.helper_flags.items() if flag is False
            ]

    def pacstrap(self, *packages, **kwargs):
        if type(packages[0]) in (list, tuple):
            packages = packages[0]

        for plugin in plugins.values():
            if hasattr(plugin, 'on_pacstrap'):
                if (result := plugin.on_pacstrap(packages)):
                    packages = result

        self.log(f'Installing packages: {packages}', level=logging.INFO)

        if (sync_mirrors := SysCommand('pacman -Syy')).exit_code == 0:
            pacstrap = SysCommand(
                f"pacstrap {self.target} {' '.join(packages)} "
                '--needed --noconfirm',
                peak_output=True)

            if pacstrap.exit_code == 0:
                return True
            else:
                self.log(f'Could not strap in packages: {pacstrap}\n'
                         f'Could not strap in packages: {pacstrap.exit_code}',
                         level=logging.ERROR,
                         fg='red')
                raise RequirementError(
                    'Pacstrap failed. '
                    'See /var/log/hyperinstaller/install.log or '
                    'above message for error details.')
        else:
            self.log(f'Could not sync mirrors: {sync_mirrors.exit_code}',
                     level=logging.INFO)

    def set_mirrors(self, mirrors):
        for plugin in plugins.values():
            if hasattr(plugin, 'on_mirrors'):
                if result := plugin.on_mirrors(mirrors):
                    mirrors = result

        return use_mirrors(
            mirrors,
            destination=f'{self.target}/etc/pacman.d/mirrorlist')

    def genfstab(self, flags='-pU'):
        self.log(f'Updating {self.target}/etc/fstab.', level=logging.INFO)

        with open(f'{self.target}/etc/fstab', 'a') as fstab_fh:
            genfstab = SysCommand(f'genfstab {flags} {self.target}').decode()
            fstab_fh.write(genfstab)

        if not os.path.isfile(f'{self.target}/etc/fstab'):
            raise RequirementError(
                'Could not generate fstab, strapping in packages most '
                f'likely failed (disk out of space?)\n{fstab}')

        for plugin in plugins.values():
            if hasattr(plugin, 'on_genfstab'):
                plugin.on_genfstab(self)

        return True

    def set_hostname(self, hostname: str, *args, **kwargs):
        if hostname == '':
            hostname = 'localhost'

        if (pathlib.Path('/etc') / 'conf.d' / 'hostname').exists():
            with open(f'{self.target}/etc/conf.d/hostname', 'w') as fh:
                fh.write('# Set to the hostname of this machine\n'
                         + 'hostname="' + hostname + '"\n')

        with open(f'{self.target}/etc/hostname', 'w') as fh:
            fh.write(hostname + '\n')

    def set_locale(self, locale, encoding='UTF-8', *args, **kwargs):
        if not len(locale):
            return True

        with open(f'{self.target}/etc/locale.gen', 'a') as fh:
            fh.write(f'{locale}.{encoding} {encoding}\n')

        with open(f'{self.target}/etc/locale.conf', 'w') as fh:
            fh.write(f'LANG={locale}.{encoding}\n')

        locale_gen = SysCommand(f'arch-chroot {self.target} locale-gen')
        return True if locale_gen.exit_code == 0 else False

    def set_timezone(self, zone, *args, **kwargs):
        if not zone:
            return True

        if not len(zone):
            return True  # Redundant

        for plugin in plugins.values():
            if hasattr(plugin, 'on_timezone'):
                if result := plugin.on_timezone(zone):
                    zone = result

        if (pathlib.Path('/usr') / 'share' / 'zoneinfo' / zone).exists():
            (pathlib.Path(self.target) / 'etc' / 'localtime').unlink(
                missing_ok=True)
            zipath = f'/usr/share/zoneinfo/{zone}'
            ltpath = '/etc/localtime'
            SysCommand(f'arch-chroot {self.target} ln -s {zipath} {ltpath}')
            return True
        else:
            self.log(f'Time zone {zone} does not exist, '
                     'continuing with system default.',
                     level=logging.WARNING,
                     fg='red')

    def activate_ntp(self):
        self.log('Installing and activating NTP.', level=logging.INFO)
        if self.pacstrap('ntp'):
            if self.enable_service('ntpd'):
                return True

    def enable_service(self, *services):
        for service in services:
            self.log(f'Enabling service {service}', level=logging.INFO)
            if (output := self.arch_chroot(
                    f'rc-update add {service} default')).exit_code != 0:
                raise ServiceException(
                    f'Unable to start service {service}: {output}')

            for plugin in plugins.values():
                if hasattr(plugin, 'on_service'):
                    plugin.on_service(service)

    def config_sndio(self):
        with open(f'{self.target}/etc/asound.conf', 'a') as asound:
            asound.write('pcm.default sndio\n')

    def run_command(self, cmd, *args, **kwargs):
        return SysCommand(f'arch-chroot {self.target} {cmd}')

    def arch_chroot(self, cmd, *args, **kwargs):
        if 'runas' in kwargs:
            cmd = f"su - {kwargs['runas']} -c \"{cmd}\""

        return self.run_command(cmd)

    def drop_to_shell(self):
        subprocess.check_call(f'arch-chroot {self.target}', shell=True)

    def configure_nic(
            self,
            nic,
            dhcp=True,
            ip=None,
            gateway=None,
            dns=None,
            *args,
            **kwargs):
        if dhcp:
            cfg = f"config_{nic}='dhcp'\n"
        else:
            assert ip

            cfg = f"config_{nic}='{ip}'\n"
            if gateway:
                cfg = cfg + f"routes_{nic}='default via {gateway}'\n"

            if dns:
                assert type(dns) == list
                cfg = cfg + f"dns_servers='{' '.join(dns)}'\n"

        self.pacstrap('netifrc')
        from os import symlink
        symlink('net.lo', f'{self.target}/etc/init.d/net.{nic}')
        self.enable_service(f'net.{nic}')

        with open(f'{self.target}/etc/conf.d/net.{nic}', 'a') as netifrc:
            netifrc.write(cfg)

    def copy_live_network_config(self, enable_services=False):
        # Copy (if any) wpa_supplicant config files
        if wpa_supplicant_cfg := glob.glob(
                '/etc/wpa_supplicant/wpa_supplicant.conf'):
            self.pacstrap('wpa_supplicant')

            if wpa_supplicant_hrc := glob.glob(
                    '/etc/conf.d/wpa_supplicant'):
                shutil.copy2(wpa_supplicant_hrc[0],
                             f'{self.target}/etc/conf.d')

            if wpa_supplicant_runit := glob.glob(
                    '/etc/sv/wpa_supplicant/conf'):
                shutil.copy2(wpa_supplicant_runit[0],
                         f'{self.target}/etc/sv/wpa_supplicant')

            shutil.copy2(wpa_supplicant_cfg[0],
                         f'{self.target}/etc/wpa_supplicant')

            if enable_services:
                self.enable_service('wpa_supplicant')

        # Copy (if any) dhcpcd config files
        if dhcpcd_cfg := glob.glob('/etc/dhcpcd.conf'):
            self.pacstrap('dhcpcd')
            shutil.copy2(dhcpcd_cfg[0], f'{self.target}/etc')

            if dhcpcd_hrc := glob.glob('/etc/conf.d/dhcpcd'):
                shutil.copy2(dhcpcd_hrc[0], f'{self.target}/etc/conf.d')

            if dhcpcd_runit := glob.glob('/etc/sv/dhcpcd/conf'):
                shutil.copy2(dhcpcd_runit[0], f'{self.target}/etc/sv/dhcpcd')

            if enable_services:
                self.enable_service('dhcpcd')

        # Copy (if any) netifrc config files
        if netifrc_cfgs := glob.glob('/etc/conf.d/net.*'):
            self.pacstrap('netifrc')
            for netifrc_cfg in netifrc_cfgs:
                shutil.copy2(netifrc_cfg, f'{self.target}/etc/conf.d')

                if enable_services:
                    self.enable_service(netifrc_cfg.split('/')[-1])

        return True

    def detect_encryption(self, partition):
        part = Partition(partition.parent, None, autodetect_filesystem=True)
        if partition.encrypted:
            return partition
        elif partition.parent not in partition.path and \
                part.filesystem == 'crypto_LUKS':
            return part

        return False

    def mkinitcpio(self, *flags):
        for plugin in plugins.values():
            if hasattr(plugin, 'on_mkinitcpio'):
                # Allow plugins to override the usage of
                # mkinitcpio altogether.
                if plugin.on_mkinitcpio(self):
                    return True

        with open(f'{self.target}/etc/mkinitcpio.conf', 'w') as mkinit:
            mkinit.write(f"MODULES=({' '.join(self.MODULES)})\n"
                         f"BINARIES=({' '.join(self.BINARIES)})\n"
                         f"FILES=({' '.join(self.FILES)})\n"
                         f"HOOKS=({' '.join(self.HOOKS)})\n")

        SysCommand(f"arch-chroot {self.target} mkinitcpio {' '.join(flags)}")

    def minimal_installation(self):
        # Add necessary packages if encrypting the drive
        # (encrypted partitions default to btrfs for now,
        #  so we need btrfs-progs)
        # TODO: Perhaps this should be living in the function which
        #       dictates the partitioning. Leaving here for now.

        for partition in self.partitions:
            if partition.filesystem == '44bsd' \
                    or partition.filesystem == '5xbsd' \
                    or partition.filesystem == 'ffs' \
                    or partition.filesystem == 'ffs2' \
                    or partition.filesystem == 'ufs' \
                    or partition.filesystem == 'ufs2':
                self.base_packages.append('ufsutils')

            if partition.filesystem == 'apfs':
                self.base_packages.append('apfs-fuse')
                self.base_packages.append('apfsprogs')

            #if partition.filesystem == 'bsd-swap':
            #    self.base_packages.append('coreutils')

            if partition.filesystem == 'btrfs':
                self.base_packages.append('btrfs-progs')

            if partition.filesystem == 'ext2' \
                    or partition.filesystem == 'ext3' \
                    or partition.filesystem == 'ext4':
                self.base_packages.append('e2fsprogs')

            if partition.filesystem == 'exfat':
                self.base_packages.append('exfatprogs')

            if partition.filesystem == 'f2fs':
                self.base_packages.append('f2fs-tools')

            if partition.filesystem == 'fat' \
                    or partition.filesystem == 'fat12' \
                    or partition.filesystem == 'fat16' \
                    or partition.filesystem == 'fat32' \
                    or partition.filesystem == 'msdos' \
                    or partition.filesystem == 'umsdos' \
                    or partition.filesystem == 'vfat':
                self.base_packages.append('dosfstools')

            #if partition.filesystem == 'hammer':
            #    self.base_packages.append('newfs_hammer')

            #if partition.filesystem == 'hammer2':
            #    self.base_packages.append('newfs_hammer2')

            if partition.filesystem == 'hfs' \
                    or partition.filesystem == 'hfs+' \
                    or partition.filesystem == 'hfs+j' \
                    or partition.filesystem == 'hfsx' \
                    or partition.filesystem == 'hfsxj':
                self.base_packages.append('hfsprogs')

            if partition.filesystem == 'jfs' \
                    or partition.filesystem == 'jfs2' \
                    or partition.filesystem == 'os2-jfs2':
                self.base_packages.append('jfsutils')

            #if partition.filesystem == 'lfs':
            #    self.base_packages.append('newfs_lfs')

            #if partition.filesystem == 'linux-swap':
            #    self.base_packages.append('util-linux')

            if partition.filesystem == 'nilfs' \
                    or partition.filesystem == 'nilfs2':
                self.base_packages.append('nilfs-utils')

            if partition.filesystem == 'ntfs':
                self.base_packages.append('ntfs-3g')

            if partition.filesystem == 'reiser4':
                self.base_packages.append('reiserfsprogs')

            if partition.filesystem == 'reiserfs':
                self.base_packages.append('reiserfsprogs')

            if partition.filesystem == 'xfs':
                self.base_packages.append('xfsprogs')

            # Configure mkinitcpio to handle some specific use cases.
            if partition.filesystem == 'btrfs':
                if 'btrfs' not in self.MODULES:
                    self.MODULES.append('btrfs')

                if 'btrfs-progs' not in self.BINARIES:
                    self.BINARIES.append('btrfs')

            if self.detect_encryption(partition):
                if 'encrypt' not in self.HOOKS:
                    self.HOOKS.insert(
                        self.HOOKS.index('filesystems'),
                        'encrypt')

        self.pacstrap(self.base_packages)
        self.helper_flags['base-strapped'] = True

        with open(f'{self.target}/etc/fstab', 'a') as fstab:
            # Redundant \n at the start? who knows?
            fstab.write('\ntmpfs /tmp tmpfs defaults,noatime,mode=1777 0 0\n')

        # TODO: Support locale and timezone
        # os.remove(f'{self.target}/etc/localtime')
        # sys_command(f'arch-chroot {self.target} '
        #             f'ln -s /usr/share/zoneinfo/{localtime} /etc/localtime')
        # sys_command('arch-chroot /mnt hwclock --hctosys --localtime')
        self.set_hostname('hyperinstaller')
        self.set_locale('en_US')

        # TODO: Use python functions for this
        SysCommand(f'arch-chroot {self.target} chmod 700 /root')

        self.mkinitcpio('-P')

        self.helper_flags['base'] = True

        # Run registered post-install hooks
        for function in self.post_base_install:
            self.log(f'Running post-installation hook: {function}',
                     level=logging.INFO)
            function(self)

        for plugin in plugins.values():
            if hasattr(plugin, 'on_install'):
                plugin.on_install(self)

        return True

    def add_bootloader(self, bootloader='grub'):
        for plugin in plugins.values():
            if hasattr(plugin, 'on_add_bootloader'):
                # Allow plugins to override the boot-loader handling.
                # This allows for bot configuring and installing
                # bootloaders.
                if plugin.on_add_bootloader(self):
                    return True

        boot_partition = None
        root_partition = None
        for partition in self.partitions:
            if partition.mountpoint == self.target + '/boot':
                boot_partition = partition
            elif partition.mountpoint == self.target:
                root_partition = partition

        if boot_partition is None and root_partition is None:
            raise ValueError('Could not detect root (/) or boot (/boot) in '
                             f'{self.target} based on: {self.partitions}')

        self.log('Adding bootloader {} to {}'.format(
                     bootloader,
                     boot_partition if boot_partition else root_partition),
                 level=logging.INFO)

        if bootloader == 'kernel':
            if has_uefi():
                self.pacstrap('efibootmgr')
                #SysCommand(f'arch-chroot {self.target} '
                #           'efibootmgr --create --disk /dev/sdX --part Y '
                #           '--loader /vmlinuz-linux-libre-lts --unicode '
                #           'root=PARTUUID='
                #           'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX rw '
                #           'initrd=\initramfs-linux-libre-lts.img '
                #           '--label "Hyperbola" --verbose')
                #SysCommand(f'arch-chroot {self.target} '
                #           'efibootmgr --create --disk /dev/sdX --part Y '
                #           '--loader /vmlinuz-linux-libre-lts --unicode '
                #           'root=PARTUUID='
                #           'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX rw '
                #           'initrd=\initramfs-linux-libre-lts-fallback.img '
                #           '--label "Hyperbola (fallback mode)" --verbose')
            else:
                raise RequirementError('BIOS cannot load kernel directly '
                                       '(except if kernel is 16bit binary)')

            self.helper_flags['bootloader'] = True
        elif bootloader == 'grub':
            self.pacstrap('grub')

            if real_device := self.detect_encryption(root_partition):
                _file = '/etc/default/grub'
                root_uuid = SysCommand('blkid -s UUID -o value {}'.format(
                    real_device.path)).decode().rstrip()
                add_to_CMDLINE_LINUX = \
                    'sed -i \'s|GRUB_CMDLINE_LINUX=""|GRUB_CMDLINE_LINUX=' \
                    f'"cryptdevice=UUID={root_uuid}:cryptlvm"|\''
                enable_CRYPTODISK = 'sed -i ' \
                    "'s|#GRUB_ENABLE_CRYPTODISK=y|GRUB_ENABLE_CRYPTODISK=y|'"

                SysCommand(f'arch-chroot {self.target} '
                           f'{add_to_CMDLINE_LINUX} {_file}')
                SysCommand(f'arch-chroot {self.target} '
                           f'{enable_CRYPTODISK} {_file}')

            if has_uefi():
                self.pacstrap('efibootmgr')
                SysCommand(f'arch-chroot {self.target} '
                           'grub-install --target=x86_64-efi '
                           '--efi-directory=/boot --bootloader-id=boot')
                SysCommand(f'arch-chroot {self.target} '
                           'cp /boot/efi/boot/grubx64.efi '
                           '/boot/efi/boot/bootx64.efi')
                SysCommand(f'arch-chroot {self.target} '
                           'grub-mkconfig -o /boot/grub/grub.cfg')
                #SysCommand(f'arch-chroot {self.target} '
                #           'efibootmgr --create --disk /dev/sdX --part Y '
                #           '--loader /efi/boot/grubx64.efi '
                #           '--label "GNU GRUB" --verbose')
            else:
                SysCommand(f'arch-chroot {self.target} '
                           'grub-install --target=i386-pc')
                SysCommand(f'arch-chroot {self.target} '
                           'grub-mkconfig -o /boot/grub/grub.cfg')

            self.helper_flags['bootloader'] = True
        elif bootloader == 'syslinux':
            self.pacstrap('syslinux')

            # incomplete: syslinux requires to configure manually

            if has_uefi():
                self.pacstrap('efibootmgr')
                SysCommand(f'arch-chroot {self.target} '
                           f'mkdir /boot/efi/boot')
                SysCommand(f'arch-chroot {self.target} '
                           'cp /usr/lib/syslinux/efi64/* /boot/efi/boot')
                SysCommand(f'arch-chroot {self.target} '
                           'cp /boot/efi/boot/syslinux.efi '
                           '/boot/efi/boot/bootx64.efi')
                #SysCommand(f'arch-chroot {self.target} '
                #           'efibootmgr --create --disk /dev/sdX --part Y '
                #           '--loader /efi/boot/syslinux.efi '
                #           '--label "SYSLINUX" --verbose')
            else:
                SysCommand(f'arch-chroot {self.target} '
                           'mkdir /boot/syslinux')
                SysCommand(f'arch-chroot {self.target} '
                           'cp /usr/lib/syslinux/bios/*.c32 /boot/syslinux')
                SysCommand(f'arch-chroot {self.target} '
                           'extlinux --install /boot/syslinux')

            self.helper_flags['bootloader'] = True
        else:
            raise RequirementError('Unknown (or not yet implemented) '
                                   f'bootloader requested: {bootloader}')

        return True

    def add_additional_packages(self, *packages):
        return self.pacstrap(*packages)

    def install_profile(self, profile):
        storage['installation_session'] = self

        if type(profile) == str:
            profile = Profile(self, profile)

        self.log(f'Installing network profile {profile}', level=logging.INFO)
        return profile.install()

    def enable_doas(self, entity: str, group=False):
        self.log(f'Enabling doas permissions for {entity}.',
                 level=logging.INFO)
        with open(f'{self.target}/etc/doas.conf', 'a') as doas:
            doas.write(f"permit nopass {':' if group else ''}{entity}\n")
        return True

    def user_create(self, user: str, password=None, groups=None, doas=False):
        if groups is None:
            groups = (
                'audio',
                'floppy',
                'input',
                'lp',
                'mail',
                'network',
                'optical',
                'power',
                'scanner',
                'storage',
                'users',
                'video'
                )

        # This plugin hook allows for the plugin to handle the
        # creation of the user.
        # Password and Group management is still handled by
        # user_create()
        handled_by_plugin = False
        for plugin in plugins.values():
            if hasattr(plugin, 'on_user_create'):
                if result := plugin.on_user_create(user):
                    handled_by_plugin = result

        if not handled_by_plugin:
            self.log(f'Creating user {user}', level=logging.INFO)
            o = b''.join(
                SysCommand(
                    f'arch-chroot {self.target} useradd -m {user}'))

        if password:
            self.user_set_pw(user, password)

        if groups:
            for group in groups:
                o = b''.join(
                    SysCommand(f'arch-chroot {self.target} '
                               f'gpasswd -a {user} {group}'))

        if doas:
            self.pacstrap('opendoas')

            sgroups = ('adm', 'log')
            for sgroup in sgroups:
                o = b''.join(
                    SysCommand(f'arch-chroot {self.target} '
                               f'gpasswd -a {user} {sgroup}'))

            if password:
                o = b''.join(
                    SysCommand(f'arch-chroot {self.target} '
                               f'gpasswd -a {user} wheel'))
            else:
                self.enable_doas(user)

            self.helper_flags['user'] = True

    def user_set_pw(self, user, password):
        self.log(f'Setting password for {user}', level=logging.INFO)

        if user == 'root':
            # This means the root account isn't
            # locked/disabled with * in /etc/passwd
            self.helper_flags['user'] = True

        o = b''.join(
            SysCommand(f'arch-chroot {self.target} '
                       f'sh -c "echo \'{user}:{password}\' | chpasswd"'))
        pass

    def user_set_shell(self, user, shell):
        self.log(f'Setting shell for {user} to {shell}', level=logging.INFO)

        o = b''.join(
            SysCommand(
                f'arch-chroot {self.target} sh -c "chsh -s {shell} {user}"'))
        pass

    def set_cui_keyboard_language(self, language: str) -> bool:
        if len(language.strip()):
            if not verify_cui_keyboard_layout(language):
                self.log(f'Invalid CUI-keyboard language specified: {language}',
                         fg='red',
                         level=logging.ERROR)
                return False

            with open(f'{self.target}/etc/conf.d/consolefont', 'w') as \
                    consolefont:
                consolefont.write('consolefont="lat9w-16"\n'
                                  'consoletranslation="8859-15_to_uni"\n'
                                  'unicodemap="lat9w"\n')

            with open(f'{self.target}/etc/conf.d/keymaps', 'w') as keymaps:
                keymaps.write(f'keymap="{language}"\n')

            self.log('CUI-Keyboard language for this '
                     f'installation is now set to: {language}')
        else:
            self.log('CUI-Keyboard language was not changed from default '
                     '(no language specified).',
                     fg='yellow',
                     level=logging.INFO)

        return True

    def set_gui_keyboard_language(self, language: str) -> bool:
        """
        A fallback function to set GUI (X11) layout specifically and
        separately from console layout.
        This isn't strictly necessary since .set_cui_keyboard_language()
        does this as well.
        """
        if len(language.strip()):
            if not verify_gui_keyboard_layout(language):
                self.log(f'Invalid GUI-keyboard language specified: {language}',
                         fg='red',
                         level=logging.ERROR)
                return False

            with open(f'{self.target}/etc/X11/xorg.conf.d/00-keyboard.conf',
                      'w') as keyboard:
                keyboard.write('Section "InputClass"\n'
                               '\tIdentifier\t"keyboard catchall"\n'
                               '\tMatchIsKeyboard\t"yes"\n'
                               '\tDriver\t"libinput"\n'
                               '\tOption\t\t"XkbLayout"\t"{language}"\n'
                               '\tOption\t\t"XkbModel"\t"pc105"\n'
                               '\tOption\t\t"XkbOptions"\t'
                               '"compose:rwin,lv3:ralt_switch,numpad:pc"\n'
                               'EndSection\n')

            self.log('GUI-Keyboard language for this '
                     f'installation is now set to: {language}')
        else:
            self.log('GUI-Keyboard language was not changed from default '
                     '(no language specified).',
                     fg='yellow',
                     level=logging.INFO)

        return True
