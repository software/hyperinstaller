import os
import subprocess
import logging

from .exceptions import ServiceException
from .general import SysCommand
from .output import log


def list_cui_keyboard_languages():
    locale_dir = '/usr/share/kbd/keymaps/'

    if not os.path.isdir(locale_dir):
        raise RequirementError(
            f'Directory containing locales does not exist: {locale_dir}')

    for root, folders, files in os.walk(locale_dir):
        for file in files:
            if os.path.splitext(file)[1] == '.gz':
                yield file.strip('.gz').strip('.map')


def list_gui_keyboard_languages():
    for line in SysCommand(
            'sed '
            "-e '/! layout/,/^$/!d' "
            "-e '/! layout/d' "
            "-e '/^$/d' "
            "-e 's|^[ ][ ]||' "
            "-e 's|[ ].\+||' "
            '< /usr/share/X11/xkb/rules/base.lst | sort'):
        yield line.decode('UTF-8').strip()


def verify_cui_keyboard_layout(layout):
    for language in list_cui_keyboard_languages():
        if layout.lower() == language.lower():
            return True

    return False


def verify_gui_keyboard_layout(layout):
    for language in list_gui_keyboard_languages():
        if layout.lower() == language.lower():
            return True

    return False


def search_cui_keyboard_layout(layout):
    for language in list_cui_keyboard_languages():
        if layout.lower() in language.lower():
            yield language


def set_cui_keyboard_language(locale):
    if len(locale.strip()):
        if not verify_cui_keyboard_layout(locale):
            log(f'Invalid keyboard locale specified: {locale}',
                fg='red',
                level=logging.ERROR)
            return False

        return subprocess.call(['loadkeys', locale]) == 0

    return False
