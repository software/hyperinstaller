import hyperinstaller

#     'gnu-free-fonts',
__packages__ = [
    'awesome',
    'xenocara-xrandr',
    'xterm',
    'feh',
    'slock',
    'terminus-font',
    'ttf-liberation',
    'xsel',
    ]

hyperinstaller.storage['installation_session'].install_profile('xenocara')

hyperinstaller.storage['installation_session'].add_additional_packages(
    __packages__)

with open(hyperinstaller.storage['installation_session'].target
          + '/etc/X11/xinit/xinitrc',
          'r') as xinitrc:
    xinitrc_data = xinitrc.read()

for line in xinitrc_data.split('\n'):
    if 'twm &' in line:
        xinitrc_data = xinitrc_data.replace(line, f'# {line}')
    if 'xclock' in line:
        xinitrc_data = xinitrc_data.replace(line, f'# {line}')
    if 'xterm' in line:
        xinitrc_data = xinitrc_data.replace(line, f'# {line}')

xinitrc_data += '\n'
xinitrc_data += 'exec awesome\n'

with open(hyperinstaller.storage['installation_session'].target
          + '/etc/X11/xinit/xinitrc',
          'w') as xinitrc:
    xinitrc.write(xinitrc_data)
