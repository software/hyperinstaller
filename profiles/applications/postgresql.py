import hyperinstaller

# Define the package list in order for lib to source
# which packages will be installed by this profile
__packages__ = ['postgresql']

hyperinstaller.storage['installation_session'].add_additional_packages(
    __packages__)

hyperinstaller.lib.general.SysCommand(
    f"arch-chroot {hyperinstaller.storage['installation_session'].target} "
    'su -c "initdb -D \'/var/lib/postgres/data\'" postgres')

hyperinstaller.storage['installation_session'].enable_service('postgresql')
