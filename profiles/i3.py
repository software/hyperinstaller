# Common package for i3,
# lets user select which i3 configuration they want.

import hyperinstaller

is_top_level_profile = False

# New way of defining packages for a profile,
# which is iterable and can be used out side of the
# profile to get a list of "what packages will be installed".
__packages__ = [
    'slim',
    'i3lock',
    'i3status',
    'i3blocks',
    'xterm',
    'dmenu',
    ]


def _prep_function(*args, **kwargs):
    """
    Magic function called by the importing installer
    before continuing any further. It also avoids executing any
    other code in this stage. So it's a safe way to ask the user
    for more input before any other installer steps start.
    """
    supported_configurations = ['i3-wm', 'i3-gaps']
    hyperinstaller.print_large_list(supported_configurations, margin_bottom=1)
    desktop = hyperinstaller.generic_select(
        supported_configurations,
        allow_empty_input=False,
        input_text='Select your desired configuration: ',
        options_output=False,
        sort=True)

    # Temporarily store the selected desktop profile
    # in a session-safe location, since this module will get reloaded
    # the next time it gets executed.
    hyperinstaller.storage['_i3_configuration'] = desktop

    # i3 requires a functional Xenocara installation.
    profile = hyperinstaller.Profile(None, 'xenocara')
    with profile.load_instructions(namespace='xenocara.py') as imported:
        if hasattr(imported, '_prep_function'):
            return imported._prep_function()
        else:
            print('Deprecated (??): '
                  'xenocara profile has no _prep_function() anymore')


# Ensures that this code only gets executed if executed through
# importlib.util.spec_from_file_location(
#     'i3',
#     '/somewhere/i3.py')
# or through conventional import i3.
if __name__ == 'i3':
    """
    This "profile" is a meta-profile.
    There are no desktop-specific steps,
    it simply routes the installer to whichever
    desktop environment/window manager was chosen.

    Maybe in the future, a network configurations or
    similar things *could* be added here.

    There are plenty of
    desktop-turn-key-solutions based on Hyperbola Project,
    this is therefore just a helper to get started
    """
    # Install common packages for all i3 configurations.
    hyperinstaller.storage['installation_session'].add_additional_packages(
        __packages__[:4])

    # Install dependency profiles.
    hyperinstaller.storage['installation_session'].install_profile('xenocara')

    # gaps is installed by default so we are
    # overriding it here with slim.
    hyperinstaller.storage['installation_session'].add_additional_packages(
        __packages__[4:])

    # Auto start Slim for all users.
    hyperinstaller.storage['installation_session'].enable_service('slim')

    # install the i3 group now
    hyperinstaller.storage['installation_session'].add_additional_packages(
        hyperinstaller.storage['_i3_configuration'])
