# A desktop environment using "Awesome" window manager.

import hyperinstaller

is_top_level_profile = False

# New way of defining packages for a profile,
# which is iterable and can be used out side of the
# profile to get a list of "what packages will be installed".
#     'nemo',
#     'gpicview',
#     'maim',
#     'alacritty',
__packages__ = [
    'slim',
    'xterm',
    ]


def _prep_function(*args, **kwargs):
    """
    Magic function called by the importing installer
    before continuing any further. It also avoids executing any
    other code in this stage. So it's a safe way to ask the user
    for more input before any other installer steps start.
    """
    # Awesome WM requires a functional Xenocara installation.
    profile = hyperinstaller.Profile(None, 'xenocara')
    with profile.load_instructions(namespace='xenocara.py') as imported:
        if hasattr(imported, '_prep_function'):
            return imported._prep_function()
        else:
            print('Deprecated (??): '
                  'xenocara profile has no _prep_function() anymore')


# Ensures that this code only gets executed if executed through
# importlib.util.spec_from_file_location(
#     'awesome',
#     '/somewhere/awesome.py')
# or through conventional import awesome.
if __name__ == 'awesome':
    # Install the application awesome from the
    # template under "/applications/".
    awesome = hyperinstaller.Application(
        hyperinstaller.storage['installation_session'],
        'awesome')
    awesome.install()

    # Install the Awesome packages.
    hyperinstaller.storage['installation_session'].add_additional_packages(
        __packages__)

    # Auto start Slim for all users.
    hyperinstaller.storage['installation_session'].enable_service('slim')

    """
    # TODO: Copy a full configuration to
    #       "~/.config/awesome/rc.lua" instead.
    with open(hyperinstaller.storage['installation_session'].target
              + '/etc/xdg/awesome/rc.lua',
              'r'
              ) as fh:
        awesome_lua = fh.read()
    """

    """
    # Replace xterm with alacritty for a smoother experience.
    awesome_lua = awesome_lua.replace('"xterm"', '"alacritty"')

    with open(hyperinstaller.storage['installation_session'].target
              + '/etc/xdg/awesome/rc.lua',
              'w'
              ) as fh:
              fh.write(awesome_lua)
    """

    # TODO: Configure the right-click-menu to contain the above
    #       packages that were installed.
    # (as a user config)

    """
    # Remove some interfering nemo settings.
    hyperinstaller.lib.general.SysCommand(
        f'arch-chroot '
        "{hyperinstaller.storage['installation_session'].target} "
        'gsettings set org.nemo.desktop show-desktop-icons false')
    hyperinstaller.lib.general.SysCommand(
        f'arch-chroot '
        "{hyperinstaller.storage['installation_session'].target} "
        'xdg-mime default nemo.desktop inode/directory '
        'application/x-gnome-saved-search')
    """
