import time

import hyperinstaller

hyperinstaller.storage['UPSTREAM_URL'] = \
    'https://git.hyperbola.info:50100' \
    '/software/hyperinstaller.git/plain/profiles'
hyperinstaller.storage['PROFILE_DB'] = 'index.json'

for name, info in hyperinstaller.list_profiles().items():
    # Tailored means it's a match for this machine
    # based on it's MAC address (or some other criteria
    # that fits the requirements for this machine specifically).
    if info['tailored']:
        print(f'Found a tailored profile for this machine called: "{name}".'
              '\nStarting install in:')
        for i in range(10, 0, -1):
            print(f'{i}...')
            time.sleep(1)

        profile = hyperinstaller.Profile(None, info['path'])
        profile.install()
        break
