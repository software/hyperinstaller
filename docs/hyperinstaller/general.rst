.. _hyperinstaller.helpers:

.. warning::

    | All these helper functions are mostly, if not all,
    | related to outside-installation-instructions.
    | Meaning the calls will affect your current running system
    | - and not touch your installed system.

Profile related helpers
=======================

.. autofunction:: hyperinstaller.list_profiles

Packages
========

.. autofunction:: hyperinstaller.find_package

Be .. autofunction:: hyperinstaller.find_packages

Locale related
==============

.. autofunction:: hyperinstaller.list_cui_keyboard_languages

.. autofunction:: hyperinstaller.search_cui_keyboard_layout

.. autofunction:: hyperinstaller.set_cui_keyboard_language

..
    autofunction:: hyperinstaller.Installer.set_cui_keyboard_layout

Mirrors
=======

.. autofunction:: hyperinstaller.filter_mirrors_by_region

.. autofunction:: hyperinstaller.add_custom_mirrors

.. autofunction:: hyperinstaller.insert_mirrors

.. autofunction:: hyperinstaller.use_mirrors

.. autofunction:: hyperinstaller.re_rank_mirrors

.. autofunction:: hyperinstaller.list_mirrors

Disk related
============

.. autofunction:: hyperinstaller.BlockDevice

.. autofunction:: hyperinstaller.Partition

.. autofunction:: hyperinstaller.Filesystem

.. autofunction:: hyperinstaller.device_state

.. autofunction:: hyperinstaller.all_disks

Luks (Disk encryption)
======================

.. autofunction:: hyperinstaller.luks2

Networking
==========

..
    autofunction:: hyperinstaller.getHwAddr

.. autofunction:: hyperinstaller.list_interfaces

General
=======

.. autofunction:: hyperinstaller.log

.. autofunction:: hyperinstaller.locate_binary

.. autofunction:: hyperinstaller.SysCommand

Exceptions
==========

..
    autofunction:: hyperinstaller.RequirementError

..
    autofunction:: hyperinstaller.DiskError

..
    autofunction:: hyperinstaller.ProfileError

.. autofunction:: hyperinstaller.SysCallError

..
    autofunction:: hyperinstaller.ProfileNotFound

..
    autofunction:: hyperinstaller.HardwareIncompatibilityError

..
    autofunction:: hyperinstaller.PermissionError

..
    autofunction:: hyperinstaller.UserError

..
    autofunction:: hyperinstaller.ServiceException
