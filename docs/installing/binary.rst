.. _installing.binary:

Binary executable
=================

| HyperInstaller can be compiled into a standalone executable.
| For Hyperbola GNU/Linux-libre based systems,
| there's a package for this called `hyperinstaller`_.

.. warning::

    | This is not required if you're running hyperinstaller on a pre-built ISO.
    | The installation is only required if you're creating your own
    | scripted installations.

Using pacman
------------

HyperInstaller is on the official repositories.

.. code-block:: console

    doas pacman -S hyperinstaller

Using PKGBUILD
--------------

| The `source`_ contains a `PKGBUILD` file which can be either
| copied straight off the website.

Once you've obtained the `PKGBUILD`, building it is pretty straight forward.

.. code-block:: console

    makepkg -s

| Which should produce a `hyperinstaller-X.x.z-1.pkg.tar.lz` that
| can be installed using:

.. code-block:: console

    doas pacman -U hyperinstaller-X.x.z-1.pkg.tar.lz

Manual compilation
------------------

| You can compile the source manually without using a custom mirror or
| the `PKGBUILD` that is shipped.
| Simply clone or download the source,
| and while standing in the cloned folder `./hyperinstaller`, execute:

.. code-block:: console

    nuitka  --standalone --show-progress hyperinstaller

| This requires the `nuitka`_ package as well as
| `python` to be installed locally.

.. _hyperinstaller: https://www.hyperbola.info/packages/extra/any/hyperinstaller/
.. _source: https://git.hyperbola.info:50100/software/hyperinstaller.git
.. _nuitka: https://www.hyperbola.info/packages/extra/any/nuitka/
