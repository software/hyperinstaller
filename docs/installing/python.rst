.. _installing.python:

Python library
==============

The hyperinstaller and library can be installed manually.

.. warning::

    | This is not required if you're running hyperinstaller on a pre-built ISO.
    | The installation is only required if you're creating your own
    | scripted installations.

Using pacman
------------

HyperInstaller is on the `official repositories`.

To install both the library and the hyperinstaller script:

.. code-block:: console

    doas pacman -S hyperinstaller

Or, to install just the library:

.. code-block:: console

    doas pacman -S python-hyperinstaller

.. _installing.python.manual:

Manual installation
-------------------

You can clone it, we'll clone it here.

.. code-block:: console

    git clone https://git.hyperbola.info:50100/software/hyperinstaller.git

Either you can move the folder into your project and simply do

.. code-block:: python

    import hyperinstaller

Or you can use `setuptools`_ to install it into the module path.

.. code-block:: console

    doas python setup.py install

.. _setuptools: https://pypi.org/project/setuptools/
