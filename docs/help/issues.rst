.. _help.issues:

Issue tracker & bugs
====================

Issues and bugs should be reported over at `https://issues.hyperbola.info`_.

| General questions, enhancements and security issues can be
| reported over there too.

Submitting a help ticket
========================

| When submitting a help ticket, please include the
| :code:`/var/log/hyperinstaller/install.log`.
| It can be found both on the live ISO but also in the
| installed filesystem if the base packages was strapped in.

| There are additional worker files,
| these worker files contain individual command input and output.
| These worker files are located in :code:`~/.cache/hyperinstaller/` and
| does not need to be submitted by default when submitting issues.

.. warning::

    | Worker log-files *may* contain sensitive information such as
    | **passwords** and **private information**.
    | Never submit these logs without going through them
    | manually making sure they're good for submission.
    | Or submit parts of it that's relevant to the issue itself.

.. _https://issues.hyperbola.info: https://issues.hyperbola.info
